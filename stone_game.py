import random

stoneNum = random.randint(4, 30)
print(f'Имеется кучка из {stoneNum} камней')
while stoneNum > 1:
    compStep = 31
    print("Ход пользователя. Возьмите 1, 2 или 3 камня.")
    userStep = int(input())
    stoneNum -= userStep
    print(f'Осталось {stoneNum} камней в кучке.')
    if stoneNum < 2:
        print("Пользователь выиграл. Поздравляю!")
        break
    print("Ход компьютера. Возьмите 1, 2 или 3 камня.")
    while compStep > stoneNum:
        compStep = random.randint(1, 3)
    print(f'Компьютер выбрал {compStep} камня.')
    stoneNum -= compStep
    print(f'Осталось {stoneNum} камней в кучке.')
    if stoneNum < 2:
        print("Компьютер выиграл. Повезет в следующий раз!")
        break
