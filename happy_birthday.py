from datetime import datetime, timedelta
try:
    print('Введите дату рождения в формате дд/мм/гггг чч:мм:сс')
    birthday = datetime.strptime(input(), "%d/%m/%Y %H:%M:%S")
except:
    print('Неправильный формат ввода даты')
    exit()
print(f'10 000 дней - {birthday + timedelta(days = 10000)}')
print(f'1 000 000 минут - {birthday + timedelta(minutes = 1000000)}')
print(f'1 000 000 000 секунд - {birthday + timedelta(seconds = 1000000000)}')
